<?php

/**
 * @file
 * Contains the definition of SiteManagementCodeDeploymentSystemBase.
 */

/**
 * Defines an abstract base class for systems that managed code deployments.
 */
abstract class SiteManagementCodeDeploymentSystemBase implements SiteManagementCodeDeploymentSystemInterface {

  /**
   * Stores the entity object representing the site.
   */
  protected $site_entity;

  /**
   * Constructs the code deployment system object.
   *
   * @param $site_entity
   *   An entity object with fields representing the site whose code is being
   *   deployed.
   */
  public function __construct(EntityMetadataWrapper $site_entity) {
    $this->site_entity = $site_entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getVersionControlSystem() {
    $vcs_name = $this->getVersionControlSystemName();

    if (empty($vcs_name)) {
      throw new Exception(t('No version control system was found.'));
    }

    switch ($vcs_name) {
      case 'git':
        return new SiteManagementGitVersionControlSystem($this);
        break;
      default:
        throw new Exception(t('Version control system "@vcs_name" is not recognized. Only git is supported currently.', array('@vcs_name' => $vcs_name)));
        break;
    }
  }

  /**
   * Returns a string representing the version control system used by this site.
   *
   * Currently supported options:
   * - 'git'
   */
  abstract public function getVersionControlSystemName();

  /**
   * {@inheritdoc}
   */
  public function siteFields() {
    return array();
  }

}
