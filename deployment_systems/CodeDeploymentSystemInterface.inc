<?php

/**
 * @file
 * Contains the definition of SiteManagementCodeDeploymentSystemInterface.
 */

/**
 * Defines an interface for a system that manages a site's code deployments.
 *
 * Typically this system will be an external hosting provider, and the
 * implementing class will interact with that external system for the following
 * purposes:
 * - To return information about the hosting environment and how the site is
 *   set up on it.
 * - To trigger code deployments on the system.
 *
 * Since different hosting providers provide APIs which return different kinds
 * of information, some implementing classes may not be able to return all the
 * information required by this interface on their own. For example, the
 * getSSHPrivateKey() method must return a private key for accessing the site's
 * version control system, but not all hosting providers have an API for
 * retrieving the private key. For this reason, user interface-related methods,
 * such as siteFields(), are provided which allows the implementing class to
 * attach fields to the local entity that represents the site, or otherwise
 * provide a user interface to allow administrators to input this information
 * directly.
 */
interface SiteManagementCodeDeploymentSystemInterface {

  /**
   * Returns a class representing the version control system used for the site.
   *
   * @throws Exception
   *   If a valid version control system cannot be found.
   */
  public function getVersionControlSystem();

  /**
   * Returns the URL of the code repository for the site.
   */
  public function getRepositoryUrl();

  /**
   * Returns the version control branch that the site's releases are based on.
   */
  public function getReleaseBranch();

  /**
   * Returns an SSH private key used to access the site's version control.
   *
   * @todo We probably shouldn't assume that all systems use SSH for access
   *   control (some might have username/password, etc).
   */
  public function getSSHPrivateKey();

  /**
   * Returns the URL of the production site, or an empty string if not found.
   */
  public function getProductionSiteUrl();

  /**
   * Deploys the current release branch to the production site.
   *
   * @throws Exception
   *   If the deployment request returns an error.
   */
  public function deployToProduction();

  /**
   * Returns an array of field API definitions for storing info about the site.
   */
  public function siteFields();

}
