<?php

/**
 * @file
 * Contains the definition of SiteManagementGitVersionControlSystem.
 */

/**
 * Git-specific implementation of the version control system interface.
 */
class SiteManagementGitVersionControlSystem extends SiteManagementVersionControlSystemBase implements SiteManagementVersionControlSystemInterface {

  /**
   * File which stores custom options for SSH commands needed by Git.
   */
  protected $ssh_exec_file;

  /**
   * Constructs the version control system object.
   *
   * @param $code_deployment_system
   *   An object representing a code deployment system that can return
   *   information about the version control system used to host a site's code.
   */
  public function __construct(SiteManagementCodeDeploymentSystemInterface $code_deployment_system) {
    parent::__construct($code_deployment_system);

    // Create a file to run SSH commands with custom options (we need
    // StrictHostKeyChecking=no to allow the Git checkout command to proceed
    // from the command line).
    // See http://stackoverflow.com/questions/7772190/passing-ssh-options-to-git-clone
    $this->ssh_exec_file = $this->getTemporaryDirectory() . '/ssh-' . drupal_random_key();
    touch($this->ssh_exec_file);
    drupal_chmod($this->ssh_exec_file, 0700);
    file_put_contents($this->ssh_exec_file, 'ssh -o StrictHostKeyChecking=no $*');
  }

  /**
   * Destroys the version control object.
   */
  public function __destruct() {
    parent::__destruct();
    file_unmanaged_delete($this->ssh_exec_file);
  }

  /**
   * {@inheritdoc}
   */
  public function merge($source_branch, $destination_branch) {
    // Run the Git checkout and merge.
    $checkout_directory = $this->getTemporaryDirectory() . '/checkout-' . drupal_random_key();
    $this->addGitSSHCommand('git clone @repo @directory', array('@repo' => $this->code_deployment_system->getRepositoryUrl(), '@directory' => $checkout_directory))
      ->addCommand('cd @directory', array('@directory' => $checkout_directory))
      ->addCommand('git checkout @source_branch', array('@source_branch' => $source_branch))
      ->addCommand('git pull')
      ->addCommand('git checkout @destination_branch', array('@destination_branch' => $destination_branch))
      ->addCommand('git pull')
      ->addCommand('git merge @source_branch', array('@source_branch' => $source_branch))
      ->addGitSSHCommand('git push')
      ->shellExecCommandsWithSSH();
    file_unmanaged_delete_recursive($checkout_directory);
  }

  /**
   * Adds a command that executes via Git over SSH.
   *
   * This wraps SiteManagementVersionControlSystemBase::addCommand() and causes
   * the added Git command to be properly executed over SSH, using the
   * currrently active private key file.
   *
   * See http://stackoverflow.com/questions/4565700/specify-private-ssh-key-to-use-when-executing-shell-command-with-or-without-ruby
   */
  protected function addGitSSHCommand($command, $args = array()) {
    return $this->addCommand('GIT_SSH=@ssh_exec_file ' . $command, array('@ssh_exec_file' => $this->ssh_exec_file) + $args);
  }

}
