<?php

/**
 * @file
 * Contains the definition of SiteManagementVersionControlSystemBase.
 */

/**
 * Defines an abstract base class for interacting with version control systems.
 */
abstract class SiteManagementVersionControlSystemBase implements SiteManagementVersionControlSystemInterface {

  /**
   * A SiteManagementCodeDeploymentSystemInterface object for a site.
   */
  protected $code_deployment_system;

  /**
   * An array of commands which will be sent to the version control system.
   */
  protected $commands = array();

  /**
   * File for temporarily storing a private key file.
   */
  private $private_key_file;

  /**
   * Constructs the version control system object.
   *
   * @param $code_deployment_system
   *   An object representing a code deployment system that can return
   *   information about the version control system used to host a site's code.
   */
  public function __construct(SiteManagementCodeDeploymentSystemInterface $code_deployment_system) {
    $this->code_deployment_system = $code_deployment_system;
  }

  /**
   * {@inheritdoc}
   */
  public function mergeToReleaseBranch($branch) {
    return $this->merge($branch, $this->code_deployment_system->getReleaseBranch());
  }

  /**
   * Returns the name of a temporary directory for storing code checkouts, etc.
   */
  protected function getTemporaryDirectory() {
    return file_directory_temp();
  }

  /**
   * Adds a command to be sent to the version control system.
   *
   * Example usage:
   * @code
   * $this->addCommand('git merge @branch', array('@branch' => $branch_name));
   * @endcode
   *
   * @param string $command
   *   The text of the command.
   * @param $args
   *   An optional array of dynamic arguments to substitute into the command,
   *   using @-style placeholders like Drupal's t() function. These arguments
   *   will be escaped when the command is prepared for execution by
   *   SiteManagementVersionControlSystemBase::buildCommandString(), so that
   *   the final command is safe to run on the command line.
   *
   * @return SiteManagementVersionControlSystemBase
   *   The called object (to allow method chaining).
   */
  protected function addCommand($command, $args = array()) {
    $this->commands[] = array('command' => $command, 'args' => $args);
    return $this;
  }

  /**
   * Builds a string for executing the current set of commands via the shell.
   *
   * @return
   *   A string suitable for passing in to shell_exec().
   */
  protected function buildCommandString() {
    $command_strings = array();
    foreach ($this->commands as $command) {
      $args = $command['args'];
      // Always escape the arguments (the @-style placeholder recommended in
      // the addCommand() function documentation is just for code style).
      $args = array_map('escapeshellarg', $args);
      $command_strings[] = strtr($command['command'], $args);
    }

    // Build the final string (each command is separated by a semicolon).
    $command_string = implode('; ', $command_strings);
    if ($command_string) {
      $command_string .= ';';
    }

    return $command_string;
  }

  /**
   * Executes the current set of commands after adding the site's private key.
   */
  protected function shellExecCommandsWithSSH() {
    $command_string = $this->buildCommandString();
    $keyfile = $this->createPrivateKeyFile();
    $command_string = 'ssh-agent bash -c "ssh-add ' . escapeshellarg($keyfile) . '; ' . $command_string . '"';
    shell_exec($command_string);
    $this->destroyPrivateKeyFile();
  }

  /**
   * Creates a temporary private key file for accessing the version control system.
   *
   * @return
   *   The name of the private key file.
   */
  protected function createPrivateKeyFile() {
    // Create the key file if it doesn't exist yet.
    if (!$this->private_key_file) {
      $this->private_key_file = $this->getTemporaryDirectory() . '/sitekey-' . drupal_random_key();
      touch($this->private_key_file);
    }

    // Write the private key to the file.
    $private_key = $this->code_deployment_system->getSSHPrivateKey();
    drupal_chmod($this->private_key_file, 0600);
    file_put_contents($this->private_key_file, $private_key);
    drupal_chmod($this->private_key_file, 0400);

    return $this->private_key_file;
  }

  /**
   * Destroys the current private key file.
   *
   * @return
   *   TRUE if the file was deleted or did not exist (and therefore did not
   *   need to be deleted), or FALSE if there was an error deleting it.
   */
  protected function destroyPrivateKeyFile() {
    if ($this->private_key_file) {
      $result = file_unmanaged_delete($this->private_key_file);
      $this->private_key_file = NULL;
      return $result;
    }
    else {
      return TRUE;
    }
  }

  /**
   * Destroys the version control object.
   */
  public function __destruct() {
    // Make sure any private key file used by this object is removed from the
    // filesystem (if it wasn't removed already), since it contains very
    // sensitive information.
    $this->destroyPrivateKeyFile();
  }

}
