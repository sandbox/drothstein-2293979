<?php

/**
 * @file
 * Contains the definition of SiteManagementVersionControlSystemInterface.
 */

/**
 * Defines an interface for interacting with a version control system.
 */
interface SiteManagementVersionControlSystemInterface {

  /**
   * Merges one branch into another.
   *
   * @param $source_branch
   *   The branch to merge from.
   * @param $destination_branch
   *   The branch to merge to.
   */
  public function merge($source_branch, $destination_branch);

  /**
   * Merges a branch into the current release branch.
   *
   * @param $branch
   *   The branch to merge from.
   */
  public function mergeToReleaseBranch($branch);

}
